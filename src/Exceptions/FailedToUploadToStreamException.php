<?php

namespace Codebase\CloudflareStream\Exceptions;

class FailedToUploadToStreamException extends \Exception
{
}