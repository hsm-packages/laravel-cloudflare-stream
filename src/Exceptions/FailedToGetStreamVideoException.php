<?php

namespace Codebase\CloudflareStream\Exceptions;

class FailedToGetStreamVideoException extends \Exception
{
}