<?php

namespace Codebase\CloudflareStream;


use Codebase\CloudflareStream\Exceptions\FailedToGetStreamVideoException;
use Codebase\CloudflareStream\Exceptions\FailedToUploadToStreamException;
use Codebase\CloudflareStream\Models\StreamVideo;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Http;
use TusPhp\Tus\Client;

class CloudflareStream
{
    protected string $account_id;
    protected string $token;

    public function __construct()
    {
        $this->loadConfig();
    }

    /**
     * @throws FailedToUploadToStreamException
     */
    public function uploadFromUrl(string $url, $name, array $meta = []): ?StreamVideo
    {
        $result = $this->request()
                       ->asJson()
                       ->post('stream/copy', ['url' => $url, 'meta' => ['name' => $name] + $meta]);

        $this->failedToUpload($result);


        return StreamVideo::fromRequest($result->json('result'));
    }

    /**
     * @throws FailedToUploadToStreamException
     */
    public function uploadFromFile(UploadedFile $file, $name, array $meta = []): ?StreamVideo
    {
        if ($file->getSize() <= (200 * 1024 * 1024)) {
            return $this->uploadUsingFile($file, $name, $meta);
        }

        return $this->uploadUsingTus($file, $name);
    }

    /**
     * @throws FailedToUploadToStreamException
     * @throws \ReflectionException
     */
    public function uploadUsingTus(UploadedFile $file, $name)
    {
        $client = new Client($this->baseUrl()."/stream", [
            'headers' => [
                'Authorization'        => 'Bearer '.$this->token,
                'Upload-Metadata-name' => $name,
            ]
        ]);
        $fileSize = (int) $file->getSize();
        $key = $this->initiateTus($fileSize, $name);
        $client
            ->setApiPath('')
            ->setKey($key)
            ->file($file->getRealPath(), $file->getFilename());

        $defaultChunkSize = (50 * 1024 * 1024);

        $bytesUploaded = 0;
        $failures = 0;
        $chunkSize = $this->getTusUploadChunkSize($defaultChunkSize, $fileSize);


        do {
            try {
                $bytesUploaded = $client->upload($chunkSize);
            } catch (\Exception $e) {
//                // We likely experienced a timeout, but if we experience three in a row, then we should back off and
//                // fail so as to not overwhelm servers that are, probably, down.
                if ($failures >= 3) {
                    throw new FailedToUploadToStreamException($e->getMessage());
                }

                $failures++;
                sleep((int) pow(4, $failures)); // sleep 4, 16, 64 seconds (based on failure count)
            }
        } while ($bytesUploaded < $fileSize);

        return $this->get($key);
    }

    public function isReady(string $videoID): bool
    {
        $video = $this->get($videoID);

        return data_get($video, 'readyToStream', false) === true;
    }

    private function getTusUploadChunkSize(int $proposedChunkSize, int $fileSize): int
    {
        $proposedChunkSize = ($proposedChunkSize <= 0) ? 1 : $proposedChunkSize;
        $chunks = floor($fileSize / $proposedChunkSize);
        $dividesEvenly = $fileSize % $proposedChunkSize === 0;
        $numberOfChunksProposed = ($dividesEvenly) ? $chunks : $chunks + 1;

        if ($numberOfChunksProposed > 1024) {
            return (int) floor($fileSize / 1024) + 1;
        }

        return $proposedChunkSize;
    }

    /**
     * @throws FailedToGetStreamVideoException
     */
    public function get(string $videoID): ?StreamVideo
    {
        $result = $this->request()->get('stream/'.$videoID);

        if (! $result->ok() || ! $result->json('success', false)) {
            throw new FailedToGetStreamVideoException(
                $result->json('errors.0.message'), $result->json('errors.0.code')
            );
        }

        return StreamVideo::fromRequest($result->json('result'));
    }

    /**
     * @return StreamVideo[]
     * @throws FailedToGetStreamVideoException
     */
    public function list($page = 1): array
    {
        //todo need pagination support
        $result = $this->request()->get('stream', ['page' => $page]);

        if (! $result->ok() || ! $result->json('success', false)) {
            throw new FailedToGetStreamVideoException(
                $result->json('errors.0.message'), $result->json('errors.0.code')
            );
        }
        $list = [];
        foreach ($result->json('result') as $item) {
            $list[] = StreamVideo::fromRequest($item);
        }

        return $list;
    }

    public function delete(string $videoID): bool
    {
        return $this->request()->delete('stream/'.$videoID)->ok();
    }

    public function request(): PendingRequest
    {
        return Http::withToken($this->token)->baseUrl($this->baseUrl());
    }

    public function loadConfig(): void
    {
        $config = config('cloudflare-stream');
        $this->account_id = $config['account_id'];
        $this->token = $config['api_token'];
        if (blank($this->account_id) || blank($this->token)) {
            throw new \Exception("Please set Cloudflare config in .env file");
        }
    }

    protected function failedToUpload(Response $result): void
    {
        if (! $result->ok() || ! $result->json('success', false)) {
            throw new FailedToUploadToStreamException(
                $result->json('errors.0.message'), $result->json('errors.0.code')
            );
        }
    }

    public function uploadUsingFile(UploadedFile $file, $name, array $meta)
    {
        $result = $this->request()
                       ->attach('file', $file->getContent(), $file->getFilename())
                       ->post('stream');
        $this->failedToUpload($result);

        //todo set meta data after upload

        return StreamVideo::fromRequest($result->json('result'));
    }

    public function metaToHeader(array $meta): string
    {
        $result = [];
        foreach ($meta as $key => $value) {
            $result[] = $key." ".$value;
        }

        return implode(',', $result);
    }

    protected function baseUrl(): string
    {
        return "https://api.cloudflare.com/client/v4/accounts/{$this->account_id}/";
    }

    public function creatorUpload(int $fileSize, $name)
    {
        $result = $this
            ->request()
            ->asJson()
            ->withHeaders([
                'Tus-Resumable'   => '1.0.0',
                'Upload-Length'   => $fileSize,
                'Upload-Metadata' => 'name '.$name.',filename '.$name
            ])
            ->post('stream?direct_user=true');
        if ($result->status() !== 201) {
            throw new FailedToUploadToStreamException(
                $result->json('errors.0.message'), $result->json('errors.0.code')
            );
        }

        return [
            'id'  => $result->header('stream-media-id'),
            'url' => $result->header('Location')
        ];
    }

    private function initiateTus(int $fileSize, $name)
    {
        $result = $this
            ->request()
            ->asJson()
            ->withHeaders([
                'Tus-Resumable'   => '1.0.0',
                'Upload-Length'   => $fileSize,
                'Upload-Metadata' => 'name '.$name.','
            ])
            ->post('stream');
        if ($result->status() !== 201) {
            throw new FailedToUploadToStreamException(
                $result->json('errors.0.message'), $result->json('errors.0.code')
            );
        }

        return $result->header('stream-media-id');
    }
}
