<?php

namespace Codebase\CloudflareStream\Facades;

use Codebase\CloudflareStream\CloudflareStream;
use Codebase\CloudflareStream\Models\StreamVideo;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Facade;

/**
 * @method static StreamVideo|null uploadFromFile(UploadedFile $video, string $name, array $meta = [])
 * @method static StreamVideo|null uploadFromUrl(string $video, string $name, array $meta = [])
 * @method static bool isReady(string $videoID)
 * @method static StreamVideo|null get(string $videoID)
 * @method static bool delete(string $videoID)
 * @method static StreamVideo[] list()
 * @method static array creatorUpload(int $size, $name)
 * @see \Codebase\CloudflareStream\CloudflareStream
 */
class CloudflareStreamFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return CloudflareStream::class;
    }
}
