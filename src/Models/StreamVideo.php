<?php

namespace Codebase\CloudflareStream\Models;

use JetBrains\PhpStorm\ArrayShape;

class StreamVideo
{
    public string $uid;
    public ?string $creator;
    public string $thumbnail;
    public int $thumbnailTimestampPct;
    public bool $readyToStream;
    public array $status;
    public array $meta;
    public string $created;
    public string $modified;
    public int $size;
    public string $preview;
    public array $allowedOrigins;
    public bool $requireSignedURLs;
    public string $uploaded;
    public ?string $uploadExpiry;
    public ?int $maxSizeBytes;
    public ?int $maxDurationSeconds;
    public int $duration;
    public array $input;
    #[ArrayShape(['hls' => 'string', 'dash' => 'string'])]
    public array $playback;
    public ?string $watermark;
    public ?string $clippedFrom;
    public ?array $publicDetails;


    public static function fromRequest(array|null $json): ?self
    {
        if ($json === null) {
            return null;
        }
        $obj = new self();
        foreach ($json as $key => $value) {
            if (property_exists($obj, $key)) {
                $obj->$key = $value;
            }
        }

        return $obj;
    }
}