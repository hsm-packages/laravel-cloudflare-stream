<?php

namespace Codebase\CloudflareStream;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class CloudflareStreamServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('cloudflare-stream')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_cloudflare_stream_table');
    }
}
