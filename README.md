# Laravel Cloudflare Stream

This package is a collection of online payment gateways that handle each gateway process

## Installation

You can install the package via composer:

```bash
composer require codebase/cloudflare-stream
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --tag="cloudeflare-stream-migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --tag="cloudeflare-stream-config"
```

This is the contents of the published config file:

```php
return [
];
```

Optionally, you can publish the views using

```bash
php artisan vendor:publish --tag="cloudeflare-stream-views"
```

## Usage

```php

```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Credits

- [hisham fawaz](https://github.com/hsmfawaz)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
