<?php

use Codebase\CloudflareStream\Facades\CloudflareStreamFacade;
use Codebase\CloudflareStream\Models\StreamVideo;
use Illuminate\Http\UploadedFile;

it('can delete video', function () {
    $result = CloudflareStreamFacade::uploadFromUrl(
        "https://storage.googleapis.com/stream-example-bucket/video.mp4", "test from url"
    );
    expect($result)->toBeInstanceOf(StreamVideo::class);
    $result = CloudflareStreamFacade::delete(
        $result->uid
    );
    expect($result)->toBeTrue();
});

it('can upload using link', function () {
    $result = CloudflareStreamFacade::uploadFromUrl(
        "https://storage.googleapis.com/stream-example-bucket/video.mp4", "test from url"
    );
    expect($result)->toBeInstanceOf(StreamVideo::class);
    CloudflareStreamFacade::delete(
        $result->uid
    );
});
//it('delete all', function () {
//    foreach (CloudflareStreamFacade::list() as $item) {
//        CloudflareStreamFacade::delete($item->uid);
//    }
//});

it('can get video', function () {
    $result = CloudflareStreamFacade::uploadFromUrl(
        "https://storage.googleapis.com/stream-example-bucket/video.mp4", "test from url"
    );
    expect($result)->toBeInstanceOf(StreamVideo::class);
    $result = CloudflareStreamFacade::get($result->uid);
    expect($result)->toBeInstanceOf(StreamVideo::class);
    CloudflareStreamFacade::delete(
        $result->uid
    );
});

it('can upload using uploaded file', function () {
    $video = new UploadedFile(
        __DIR__.DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."sample.mp4", 'sample.mp4', 'video/mp4'
    );
    $result = CloudflareStreamFacade::uploadFromFile($video, 'test from file', [
        'oaa' => 'asdasdasd'
    ]);
    expect($result)->not()->toBeNull();
    CloudflareStreamFacade::delete(
        $result->uid
    );
});

it('can upload using creator upload', function () {
    $video = new UploadedFile(
        __DIR__.DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."sample.mp4", 'sample.mp4', 'video/mp4'
    );
    $result = CloudflareStreamFacade::creatorUpload($video->getSize(), 'test from file');
    expect($result['id'])->not()->toBeEmpty();
    expect($result['url'])->not()->toBeEmpty();
    CloudflareStreamFacade::delete(
        $result['id']
    );
});
