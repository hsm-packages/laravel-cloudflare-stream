<?php

return [
    'account_id' => env('CLOUDFLARE_STREAM_ACCOUNT_ID', ''),
    'api_token'    => env('CLOUDFLARE_STREAM_API_TOKEN', '')
];
